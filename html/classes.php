<?php

require("html/src/Engines/Arena.php");
// require("html/src/Engines/Game.php");
require("html/src/Engines/Round.php");

require("html/src/Dal/DAO.php");
require("html/src/Dal/DAOcharacter.php");
require("html/src/Dal/DAOrace.php");
require("html/src/Dal/DAOjob.php");

// require("html/src/Models/Character.php");
require("html/src/Models/Job.php");
require("html/src/Models/Race.php");
require("html/src/Models/Stats.php");

require("html/src/Models/Interfaces/Fighter.php");

require("html/db/characters.json");
require("html/db/datas.json");
require("html/db/jobs.json");
require("html/db/races.json");

?>