<?php

// namespace Beweb\Td\Dal;

// use Beweb\Td\Models\Character;

// use Beweb\Td\Models\Character;

class DAOcharacter extends DAO
{

    function __construct()
    {
        $this->datasource = "./db/characters.json";
    }

    // la fonction persist permet de faire persister la data (nos caractères) dans le fichier json carac
    function persist(mixed $data)
    {

        // on stock dans $characters notre tableau d'objets Character ($this->load() 
        // pointe sur les objets qu'on return de la fonction load juste en dessous)
        $characters = $this->load();

        // on incrémente id de notre perso 
        $data->id = count($characters) + 1;

        // on push dans notre tableau d'objets $characters notre nouveau perso créé
        array_push($characters, $data);

        // ici on encode en json pour pouvoir serialize les données
        //  (utile pour pouvoir sélectionner ce qu'on envoi dans notre fichier json)
        file_put_contents($this->datasource, json_encode($characters));
    }


    /**
     * Ici on implémente une methode pour creer un personnage 
     * oui le DAO est un gestionnaire d'entité , il semble normal de lui deléguer 
     * la fastidieuse tache de fabriquer des personnages (oui ce serait con de faire ça dans l'index.php alors qu'on sait faire des objets ;) )
     * 
     * @todo faire en sorte de passer par un objet qui retourne le bon DAO selon ce qu'on veut faire
     * au lieu de les instanciers dans les methodes (cf : fabrique , singleton ).
     *
     * @param string $race idem que pour job mais pour  race , sauf que c'est différent parce que c'est pas pareil alors que les structures sont les mêmes, mais c'est tout a faire similaire malgré le corollaire évident de la propriété recherchée 
     * @param string $job la classe du personnage pour la chercher afin de l'ajouter a la création du personnage
     * @return Character le personnage que l'on vient de creer
     */

    // creation de carac avec en param chaine de carac race et job
    function createCharacter(string $race, string $job, string $name): Character
    {

        // ici on donne à $daoRace et $daoJob -> leurs classes et leurs méthodes spécifiques (fonctions)
        $daoRace = new DAOrace();
        $daoJob = new DAOjob();

        // nouveau carac
        $c = new Character(
            //on pointe sur les méthodes des classes DAOrace et DAOjob
            // on utilise la méthode findByName avec en entrée les 2 strings envoyé en entrée créa perso
            $daoRace->findByName($race),
            $daoJob->findByName($job),
            $name
        );
        echo "___";
        $c->att = $daoRace->findByName($race)->att * $daoJob->findByName($job)->att_multi;
        $c->def = $daoRace->findByName($race)->def * $daoJob->findByName($job)->def_multi;
        $c->hp = $daoRace->findByName($race)->hp * $daoJob->findByName($job)->hp_multi;
        echo "___";



        // on pointe sur la méthode de notre classe DAOcharacter persist()
        // on fait persister notre caractère (dans le fichier json  characters.json)
        $this->persist($c);
        return $c;
    }


    /**
     * 
     *
     * @return array - retourne un tableau d'objet de Characters
     */
    // load va retourner notre fichier json en tabeau d'objet associatif 
    // et pointer sur les id de race et job pour charger également leur objet en fonction de l'id race et job
    function load(): array
    {

        // init tableau characters
        $characters = [];

        // on stock dans la variable datas notre contenu de fichier json décodé
        // tableau associatif si TRUE (oui ici)
        $datas = json_decode(file_get_contents($this->datasource), true);

        //on charge les méthodes de DAOrace et DAOjob
        $DAOrace = new DAOrace();
        $DAOjob = new DAOjob();


        // on loop dans notrte tableau associatif datas, pour chaque élément)
        foreach ($datas as  $character_as_array) {

            //creation de notre class Character
            $c = new Character(
                // on invvoque la méthode findById, et on pointe sur la race de l'élément en question
                $DAOrace->findById($character_as_array["race"]),
                $DAOjob->findById($character_as_array["job"]),
                $character_as_array["name"],

            );

            array_push($characters, $c);
        }

        // on retourne le tableau jobs
        return $characters;
    }

    function findById(int $id): mixed
    {
        foreach ($this->load() as $character) {
            if ($character->id == $id) {
                return $character;
            }
        }
    }
    function findByName($name)
    {
        foreach ($this->load() as $key => $character) {
            if ($character['name'] == $name) {
                return $character;
            }
        }
    }


}
