<?php

// namespace Beweb\Td\Dal;

// require("html/src/Dal/DAOrace.php");

/**
 * Objet permettant d'acceder a une source de données 
 * cet objet foruni une couche d'asbstraction afin de faciliter son utilisation
 */

abstract class DAO
{

  protected string $datasource;


  function __construct($datasource)
  {
    $this->datasource = $datasource;
  }
  // data arrive dans la fonction
  abstract function persist(mixed $data);
  abstract function load(): mixed;
}
