<?php

// namespace Beweb\Td\Engines;

use Arena;
use DAOcharacter;
use DAOjob;
use DAOrace;

class Game
{
  private $arena; //array
  private bool $gameOver;

  public function __construct()
  {
    $this->arena = new Arena;
    $this->gameOver = false;
    $this->datasource = "./db/characters.json";
  }


  // la fonction persist permet de faire persister la data (nos caractères) dans le fichier json carac
  function persist(mixed $data)
  {

    // on stock dans $characters notre tableau d'objets Character ($this->load() 
    // pointe sur les objets qu'on return de la fonction load juste en dessous)
    $characters = $this->load();

    // on incrémente id de notre perso 
    $data->id = count($characters) + 1;

    // on push dans notre tableau d'objets $characters notre nouveau perso créé
    array_push($characters, $data);

    // ici on encode en json pour pouvoir serialize les données
    //  (utile pour pouvoir sélectionner ce qu'on envoi dans notre fichier json)
    file_put_contents($this->datasource, json_encode($characters));
  }

  public function add_character($qty)
  {
    // get random race from array
    function getARace()
    {
      $races = [];
      $datas = json_decode(file_get_contents("./db/races.json"), true);
      foreach ($datas as $data) {
        array_push($races, $data["name"]);
      }
      return $races[rand(0, (count($races) - 1))];
    }
    // // get random job from  array
    // function getAJob()
    // {
    //   $array = ["Warlock", "Warrior", "Druid"];
    //   return $array[rand(0, (count($array) - 1))];
    // }
    // get random job from  array
    function getAJob()
    {
      $jobs = [];
      $datas = json_decode(file_get_contents("./db/jobs.json"), true);
      foreach ($datas as $data) {
        array_push($jobs, $data["name"]);
      }
      return $jobs[rand(0, (count($jobs) - 1))];
    }


    /**
     * Loop on the number of Characters we want to create
     * number decided in index.php ($qty)
     */
    while ($qty > 0) {
      // set a new Character
      $DAOcharacter = new DAOcharacter;
      $character = $DAOcharacter->createCharacter(getARace(), getAJob(), "Joueur.$qty");
      // Set the name of the new caracter 
      //$character->showCharacterstats();

      // add the new character to the Arena->pit
      array_push($this->arena->pit, $character);
      $qty--;
    }
  }


  //>>>>>>>>>>>>>>>>>>>>>>>>   
  // START game
  //>>>>>>>>>>>>>>>>>>>>>>>>

  public function start()
  {
    $arena = $this->arena->pit;

    while ($this->gameOver != true) {

      $round = new Round($arena);

      $arena = $round->round_arena;

      if (count($arena) <= 1) {
        $this->gameOver = true;
        echo "\n" . 'we got a winner --> ' . $arena[0]->name . " !! \n";
        // affiche les infos du winner et fin de jeu
      }

      //$this->gameOver = true;
    }
  }

  //>>>>>>>>>>>>>>>>>>>>>>>>   
  // LOAD
  //>>>>>>>>>>>>>>>>>>>>>>>>

  function load(): array
  {

    // init tableau characters
    $characters = [];

    // on stock dans la variable datas notre contenu de fichier json décodé
    // tableau associatif si TRUE (oui ici)
    $datas = json_decode(file_get_contents($this->datasource), true);

    //on charge les méthodes de DAOrace et DAOjob
    $DAOrace = new DAOrace();
    $DAOjob = new DAOjob();


    // on loop dans notrte tableau associatif datas, pour chaque élément)
    foreach ($datas as  $character_as_array) {

      //creation de notre class Character
      $c = new Character(
        // on invvoque la méthode findById, et on pointe sur la race de l'élément en question
        $DAOrace->findById($character_as_array["race"]),
        $DAOjob->findById($character_as_array["job"]),
        $character_as_array["name"],

      );

      array_push($characters, $c);
    }

    // on retourne le tableau jobs
    return $characters;
  }


  //>>>>>>>>>>>>>>>>>>>>>>>>   
  // find by
  //>>>>>>>>>>>>>>>>>>>>>>>>

  function findById(int $id): mixed
  {
    foreach ($this->load() as $character) {
      if ($character->id == $id) {
        return $character;
      }
    }
  }
  function findByName($name)
  {
    foreach ($this->load() as $key => $character) {
      if ($character['name'] == $name) {
        return $character;
      }
    }
  }
}
